/**
 * Menghilangkan nilai dari Array.
 * @arrayBuah @Array campuran buah dan hewan
 * @arrayHewan @Array dari hewan
 *
 * @return sebuah @Array
 */
function hapusArray(arrayBuah, arrayHewan) {
  const arrayBuahBaru = []; //membuat Array baru

  //tempat coding disini
  arrayHewan.forEach(element => {
    var index = arrayBuah.indexOf(element);
    if (index > -1){
      arrayBuah.splice(index,1);
    }
  });
  arrayBuahBaru.push(arrayBuah);
  return arrayBuahBaru;
}
