/**
 * membuat sebuah Object dengan isian data diri
 *
 * contoh.
 * nama: Maudy
 * alamat: Bandung
 * hobi: Menyani
 * hp: 085123456789
 *
 * @return sebuah @Object data diri
 */
function membuatObject() {
  const objectSaya = {}; //membuat variabel Object

  //tempat coding disini
  objectSaya.nama = 'Ibnu';
  objectSaya.alamat = 'Bandung Barat';
  objectSaya.hobi = 'Baca Buku';
  objectSaya.hp = "082115645571"

  return objectSaya;
}
