/**
 * Membuat Object berdasarkan Array yang di kasih.
 * @dataArray ['ayam', 'bebek', 'angsa']
 *
 * @return sebuah @Object
 */
function ambilInfoArray(dataArray) {
  const dataObject = {}; //membuat variabel Object

  dataObject.data = dataArray;
  dataObject.panjangData = dataArray.length;
  

  return dataObject;
}
